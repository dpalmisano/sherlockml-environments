# RStudio Server

[RStudio Server][rstudio] is an IDE for the [R programming language][R]. This
environment installs RStudio Server, and configures it to be served instead of
the default Jupyter notebook on a SherlockML server.

[R]: https://www.r-project.org/
[rstudio]: https://www.rstudio.com/products/rstudio/
